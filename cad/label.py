# ***********************************************
# ***       Loco363 - Parts - Indicator       ***
# ***-----------------------------------------***
# *** Elektro-potkan  <git@elektro-potkan.cz> ***
# ***-----------------------------------------***
# ***                GNU GPLv3+               ***
# ***********************************************


import gen_draw
from gen_draw.coords import Coords as C

# import generic CAD (independently from working directory)
from path_loader import pathInsert
pathInsert('.', __file__)
import generic


class Indicator(generic.bases.LabelPart):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		c = self.getCoords()
		
		# outer-circle
		self.add(gen_draw.shapes.Circle(
			c,
			35,
			properties={
				'fill': self.COLOR_UNI,
				'stroke': self.color,
				'stroke-width': 3
			}
		))
		
		# inner-circle
		self.add(gen_draw.shapes.Circle(
			c,
			25,
			properties={
				'fill': 'white'
			}
		))
	# constructor
# class Indicator


if __name__ == '__main__':
	print(gen_draw.compilers.SVG(Indicator(name='IND'), True))

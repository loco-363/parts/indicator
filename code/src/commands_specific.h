/**
***********************************************
***       Loco363 - Parts - Indicator       ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * I2C Commands - specific
 */
#ifndef COMMANDS_SPECIFIC_H_
#define COMMANDS_SPECIFIC_H_


typedef enum {
	/**
	 * Returns servo position
	 * @return byte - servo position as pulse duration in 10us (e.g. 150 = 1.5ms pulse)
	 */
	CMD_SERVO_GET_POSITION = 0X20,
	
	/**
	 * Sets servo position
	 * @param byte - servo position as pulse duration in 10us (e.g. for 1.5ms pulse send 150 decimal)
	 */
	CMD_SERVO_SET_POSITION = 0X21,
	
	
	/**
	 * Returns servo PWM frequency
	 * @return byte - frequency in 10Hz (e.g. 5 = 50Hz)
	 */
	CMD_SERVO_GET_FREQUENCY = 0X22,
	
	/**
	 * Sets servo PWM frequency
	 * @param byte - frequency in 10Hz (e.g. for 50Hz send 5)
	 */
	CMD_SERVO_SET_FREQUENCY = 0X23
} COMMAND_SPECIFIC_T;


#endif

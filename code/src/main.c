/**
***********************************************
***       Loco363 - Parts - Indicator       ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


#include <avr/interrupt.h>
#include <avr/io.h>
#include <inttypes.h>

#include "usiTwiSlave.h"

#include "commands_specific.h"
#include "system.h"


volatile unsigned char servo_position = 0; // *10 us
volatile unsigned char servo_frequency = 0; // *10 Hz, 0=stopped

volatile uint16_t TIMER0_extended_counter = 0;
volatile uint16_t TIMER0_extended_counter_TOP = 0;

/**
 * Routine handling TIMER0 Overflow Interrupt
*/
ISR(TIMER0_COMPA_vect){
	if(TIMER0_extended_counter == servo_position){
		PORTB &= ~SERVO;
	};

	TIMER0_extended_counter++;

	if(TIMER0_extended_counter > TIMER0_extended_counter_TOP){
		TIMER0_extended_counter = 0;
		PORTB |= SERVO;
	};
}

/**
* Sets servo position
* @param unsigned char pos - position = pos*10 us
*/
void ServoPosition(unsigned char pos){
	servo_position = pos;
}

/**
* Sets servo frequency
* @param unsigned char freq - frequency = freq*10 Hz
*/
void ServoFrequency(unsigned char freq){
	if(freq == 0){
		TCCR0B = 0; // Timer0 stopped
		PORTB &= ~SERVO;
		TIMER0_extended_counter = 0;
	}
	else {
		TIMER0_extended_counter_TOP = 10000/freq;

		if(servo_frequency == 0){
			TCCR0B = (1 << CS01); // Timer0 runs with prescaler 8
		};
	};

	servo_frequency = freq;
}

int main(){
	DDRB = SERVO; // PB0 and PB2 configured as USI via usiTwiSlaveInit function
	PORTB = 0;
	
	TCCR0A = (1 << WGM01); // Timer0 in mode CTC (TOP = OCRA)
	TCCR0B = 0; // Timer0 stopped
	OCR0A = 10;
	TIMSK = (1 << OCIE0A);
	
	ServoPosition(150);
	ServoFrequency(5);
	
	sei(); // Enable Global interupts
	
	usiTwiSlaveInit(I2C_ADDRESS);
	
	while(1){
		if(usiTwiDataInReceiveBuffer()){
			// Process registers
			uint8_t reg = usiTwiReceiveByte();
			switch(reg){
				case CMD_SERVO_GET_POSITION:
					usiTwiTransmitByte(servo_position);
					break;
				case CMD_SERVO_GET_FREQUENCY:
					usiTwiTransmitByte(servo_frequency);
					break;
				
				case CMD_SERVO_SET_POSITION:
					ServoPosition(usiTwiReceiveByte());
					break;
				case CMD_SERVO_SET_FREQUENCY:
					ServoFrequency(usiTwiReceiveByte());
					break;
			};
		};
	};
}

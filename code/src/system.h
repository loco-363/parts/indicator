/**
***********************************************
***       Loco363 - Parts - Indicator       ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * System constants & macros
 */
#ifndef SYSTEM_H_
#define SYSTEM_H_


/** I2C */
/** 7-bit address */
#ifndef I2C_ADDRESS
	#define I2C_ADDRESS   0x30
#endif


/** Hardware */
/** Servo */
#define SERVO   (1 << PB3)


/** Check external options */
/** I2C */
#if I2C_ADDRESS < 0x00 || I2C_ADDRESS > 0x7F
	#error "I2C address has to be one-byte unsigned integer!"
#endif


#endif
